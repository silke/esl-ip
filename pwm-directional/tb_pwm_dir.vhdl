library vunit_lib;
library ieee;
library osvvm;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use osvvm.RandomPkg.all;

entity tb_pwm_dir is
  generic (runner_cfg : string  := "";
           data_width : natural := 16;
           pwm_mode   : natural := 0);
end tb_pwm_dir;

architecture tester of tb_pwm_dir is

  component pwm_dir is
    generic (BUS_WIDTH : integer);
    port(rst, clk                : in  std_logic;
         brake1, brake2          : in  std_logic;
         dir1, dir2              : in  std_logic;
         ina1, ina2, inb1, inb2  : out std_ulogic;
         pwm1, pwm2              : out std_ulogic;
         compare1, compare2, top : in  signed(BUS_WIDTH-1 downto 0);
         countmode               : in  std_logic);
  end component;

  subtype bit2 is std_ulogic_vector (1 downto 0);

  signal clk, reset, write              : std_logic                     := '0';
  signal ha1, hb1, ha2, hb2, out1, out2 : std_ulogic;
  signal comp1, comp2, top              : signed(data_width-1 downto 0) := to_signed(0, data_width);
begin
  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  pwm : pwm_dir
    generic map (BUS_WIDTH => data_width)
    port map(reset,
             clk,
             '0',
             '0',
             '0',
             '0',
             ha1, hb1, ha2, hb2,
             out1, out2,
             comp1, comp2, top,
             '0');

  main : process
    variable RV0, RV1, RV2 : RandomPType;
    variable counter       : signed(data_width-1 downto 0);
    variable change        : integer range -1 to 1;
    variable loopcount     : integer;
    variable looptime      : time;
  begin
    -- Simulation start
    test_runner_setup(runner, runner_cfg);

    -- Seed the random values
    RV0.InitSeed (RV0'instance_name);
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);

    -- Wait until the reset is done
    wait for 100 ns;

    -- Set new value and wait for update tick
    top   <= to_signed(400, top'length);
    comp1 <= to_signed(200, top'length);
    comp2 <= to_signed(150, top'length);
    wait until clk = '1';
    wait until clk = '0';
    wait until clk = '1';
    -- Simulation ends here
    wait for 200 us;
    test_runner_cleanup(runner);
  end process;
end tester;
