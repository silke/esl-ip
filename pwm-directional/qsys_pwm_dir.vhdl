library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity quad_dec is
  generic (
    DATA_WIDTH : natural := 32  -- word size of each input and output register
    );
  port (
    -- signals to connect to an Avalon clock source interface
    clk    : in std_logic;
    resetn : in std_logic;

    -- signals to connect to an Avalon-MM slave interface
    slave_address    : in  std_logic_vector(3 downto 0);
    slave_read       : in  std_logic;
    slave_write      : in  std_logic;
    slave_readdata   : out std_logic_vector(DATA_WIDTH-1 downto 0);
    slave_writedata  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    slave_byteenable : in  std_logic_vector((DATA_WIDTH/8)-1 downto 0);

    -- signals to connect to custom user logic
    pwma_out : out std_logic;
    pwmb_out : out std_logic;
    hb1a     : out std_logic;
    hb2a     : out std_logic;
    hb1b     : out std_logic;
    hb2b     : out std_logic
    );
end entity;

architecture implementation of quad_dec is
  signal new_top, compa, compb : signed(DATA_WIDTH-1 downto 0);
  signal ctla, ctlb, ctl_count : std_logic_vector(DATA_WIDTH-1 downto 0);
  alias count_mode             : std_logic is ctl_count(0);
  alias dira                   : std_logic is ctla(0);
  alias brakea                 : std_logic is ctla(1);
  alias dirb                   : std_logic is ctlb(0);
  alias brakeb                 : std_logic is ctlb(1);


  -- Definition of the pwm controller
  component pwm_dir
    generic (
      BUS_WIDTH : natural := 8
      );
    port(rst, clk                : in  std_logic;
         brake1, brake2          : in  std_logic;
         dir1, dir2              : in  std_logic;
         ina1, ina2, inb1, inb2  : out std_ulogic;
         pwm1, pwm2              : out std_ulogic;
         compare1, compare2, top : in  signed(BUS_WIDTH-1 downto 0);
         -- count mode, 0: fast, 1: phase correct
         countmode               : in  std_logic);
  end component;

begin
  -- Initialization of the example
  pd : pwm_dir
    generic map(
      BUS_WIDTH => DATA_WIDTH
      )
    port map(resetn,
             clk,
             brakea,
             brakeb,
             dira,
             dirb,
             hb1a,
             hb1b,
             hb2a,
             hb2b,
             pwma_out,
             pwmb_out,
             compa,
             compb,
             new_top,
             count_mode);

  -- Communication with the bus
  -- reg0 : count ctl
  -- reg1 : top
  -- reg2 : ctl a
  -- reg3 : comp a
  -- reg4 : ctl b
  -- reg5 : comp b

  p_avalon : process(clk, resetn)
    variable slave_addr_num : integer range 0 to 7;
  begin
    slave_addr_num := 0;
    if (resetn = '0') then
      new_top   <= (others => '0');
      compa     <= (others => '0');
      compb     <= (others => '0');
      ctla      <= (others => '0');
      ctlb      <= (others => '0');
      ctl_count <= (others => '0');
    elsif (rising_edge(clk)) then
      slave_addr_num := to_integer(signed(slave_address));
      if (slave_read = '1') then
        -- count ctl
        case slave_addr_num is
          when 0      => slave_readdata <= ctl_count;
          when 1      => slave_readdata <= std_logic_vector(new_top);
          when 2      => slave_readdata <= ctla;
          when 3      => slave_readdata <= std_logic_vector(compa);
          when 4      => slave_readdata <= ctlb;
          when 5      => slave_readdata <= std_logic_vector(compb);
          when others => slave_readdata <= (others => '0');
        end case;
      end if;
      if (slave_write = '1') then
        case slave_addr_num is
          when 0 => ctl_count <= slave_writedata;
          when 1 => new_top   <= signed(slave_writedata);
          when 2 => ctla      <= slave_writedata;
          when 3 => compa     <= signed(slave_writedata);
          when 4 => ctlb      <= slave_writedata;
          when 5 => compb     <= signed(slave_writedata);
          when others =>
        end case;
      end if;
    end if;
  end process;
end architecture;
