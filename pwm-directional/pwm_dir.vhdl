library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity pwm_dir is
  generic (BUS_WIDTH : integer := 16);

  port(rst, clk                : in  std_logic;
       brake1, brake2          : in  std_logic;
       dir1, dir2              : in  std_logic;
       ina1, ina2, inb1, inb2  : out std_ulogic;
       pwm1, pwm2              : out std_ulogic;
       compare1, compare2, top : in  signed(BUS_WIDTH-1 downto 0);
       -- count mode, 0: fast, 1: phase correct
       countmode               : in  std_logic);
end pwm_dir;

architecture implementation of pwm_dir is
  subtype bit2 is std_ulogic_vector (1 downto 0);
  constant zeroes : signed(BUS_WIDTH-1 downto 0) := (others => '0');

  signal counter                             : signed(BUS_WIDTH-1 downto 0);
  -- Buffered signals, these are updated at counter = zero
  signal buf_compare1, buf_compare2, buf_top : signed(BUS_WIDTH-1 downto 0);
  signal buf_countmode                       : std_logic;
  signal buf_dir1, buf_dir2                  : std_logic;

  component dir_ctl is
    port(brake    : in  std_logic;
         dir      : in  std_logic;
         ina, inb : out std_ulogic);
  end component;

begin
  dir_c1 : dir_ctl
    port map(
      brake1,
      buf_dir1,
      ina1,
      inb1);

  dir_c2 : dir_ctl
    port map(
      brake2,
      buf_dir2,
      ina2,
      inb2);

  process(rst, clk)
    type countdir_t is (up, down);
    variable tempcounter : signed(BUS_WIDTH-1 downto 0);
    variable countdir    : countdir_t;
  begin
    tempcounter := (others => '0');
    if rst = '0' then
      countdir := up;
      counter  <= (others => '0');
    elsif rising_edge(clk) then
      -- Check if direction should change
      if counter = zeroes then
        countdir    := up;
        tempcounter := counter + 1;
      elsif counter = buf_top then
        if buf_countmode = '1' then
          countdir    := down;
          tempcounter := counter - 1;
        else
          countdir    := up;
          tempcounter := zeroes;
        end if;
      else
        case countdir is
          when up   => tempcounter := counter + 1;
          when down => tempcounter := counter -1;
        end case;
      end if;

      -- Check if next value is zero and update buffers
      if tempcounter = zeroes then
        buf_countmode <= countmode;
        buf_top       <= top;
        buf_compare1  <= compare1;
        buf_compare2  <= compare2;
        buf_dir1      <= dir1;
        buf_dir2      <= dir2;
      end if;
      counter <= tempcounter;
    end if;
  end process;
  pwm1 <= '1' when counter < buf_compare1 else '0';
  pwm2 <= '1' when counter < buf_compare2 else '0';
end architecture;
