library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Control direction and brake outputs
entity dir_ctl is
  port(brake    : in  std_logic;
       dir      : in  std_logic;
       ina, inb : out std_ulogic);
end dir_ctl;

architecture implementation of dir_ctl is
begin
  -- level the direction signals when braking
  -- Output oposite values depending on direction signal
  ina <= '0' when brake = '1' else
         '1' when dir = '0' else '0';

  inb <= '0' when brake = '1' else
         '0' when dir = '0' else
         '1' when dir = '1' else '0';
end implementation;
