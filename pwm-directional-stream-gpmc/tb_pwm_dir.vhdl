library ieee;
library vunit_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;


entity tb_pwm_dir is
  generic (runner_cfg : string  := "";
           data_width : natural := 32;
           pwm_mode   : natural := 0);
end tb_pwm_dir;

architecture tester of tb_pwm_dir is

  component pwm_gpmc is
    generic (DATA_WIDTH : integer);
    port(
      clk    : in std_logic;
      resetn : in std_logic;

      top, lead : in signed(DATA_WIDTH-1 downto 0);


      -- signals to connect to an Avalon-ST interface
      st_data  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
      st_ready : out std_logic;
      st_valid : in  std_logic;

      -- signals to connect to custom user logic
      pwm_out : out std_logic;
      hba     : out std_logic;
      hbb     : out std_logic);
  end component;

  subtype bit2 is std_ulogic_vector (1 downto 0);

  signal clk, reset                : std_logic                     := '0';
  signal ha1, hb1, out1            : std_ulogic;
  signal lead, top                 : signed(data_width-1 downto 0) := to_signed(0, data_width);
  signal st_ready_in, st_valid_out : std_ulogic;
  signal st_data_out               : std_logic_vector(data_width-1 downto 0);

begin
  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  pwm : pwm_gpmc
    generic map (DATA_WIDTH => data_width)
    port map(clk,
             reset,
             top,
             lead,
             st_data_out,
             st_ready_in,
             st_valid_out,
             out1,
             ha1, hb1
             );

  main : process

    variable counter   : signed(data_width-1 downto 0);
    variable change    : integer range -1 to 1;
    variable loopcount : integer;
    variable looptime  : time;
  begin
    -- Simulation start
    test_runner_setup(runner, runner_cfg);
    -- Wait until the reset is done
    st_data_out  <= std_logic_vector(to_signed(0, st_data_out'length));
    st_valid_out <= '0';
    lead         <= to_signed(7, st_data_out'length);
    wait for 100 ns;

    -- Set new value and wait for update tick
    top <= to_signed(400, top'length);

    wait until clk = '1';
    wait until clk = '0';
    wait until clk = '1';
    -- Simulation ends here
    wait until st_ready_in = '1';
    st_data_out  <= std_logic_vector(to_signed(250, st_data_out'length));
    st_valid_out <= '1';
    wait for 40 us;

    wait until st_ready_in = '1';
    st_data_out  <= std_logic_vector(to_signed(-30, st_data_out'length));
    st_valid_out <= '1';
    wait for 40 us;

    test_runner_cleanup(runner);
  end process;
end tester;
