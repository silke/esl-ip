library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm_gpmc is
  generic (
    DATA_WIDTH : natural := 32  -- word size of each input and output register
    );
  port (
    -- signals to connect to an Avalon clock source interface
    clk    : in std_logic;
    resetn : in std_logic;

    top, lead : in signed(DATA_WIDTH-1 downto 0);


    -- signals to connect to an Avalon-ST interface
    st_data  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    st_ready : out std_logic;
    st_valid : in  std_logic;

    -- signals to connect to custom user logic
    pwm_out : out std_logic;
    hba     : out std_logic;
    hbb     : out std_logic
    );
end entity;

architecture implementation of pwm_gpmc is
  signal comp_out, comp : signed(DATA_WIDTH-1 downto 0);
  signal counter_out    : signed(DATA_WIDTH-1 downto 0);
  signal brake, dir     : std_logic;

  constant zero : signed(DATA_WIDTH-1 downto 0) := (others => '0');
  -- Definition of the pwm controller
  component pwm_dir
    generic (
      BUS_WIDTH : natural := 8
      );
    port(rst, clk     : in  std_logic;
         brake        : in  std_logic;
         dir          : in  std_logic;
         ina, inb     : out std_ulogic;
         pwm          : out std_ulogic;
         compare, top : in  signed(BUS_WIDTH-1 downto 0);
         counter_out  : out signed(BUS_WIDTH-1 downto 0));
  end component;

begin
  -- Initialization of the example
  pd : pwm_dir
    generic map(
      BUS_WIDTH => DATA_WIDTH
      )
    port map(resetn,
             clk,
             brake,
             dir,
             hba,
             hbb,
             pwm_out,
             comp_out,
             top,
             counter_out);

  -- Avalon ST bus, sink side
  -- https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/manual/mnl_avalon_spec.pdf
  p_avalon_st : process(clk, resetn)
    variable ready : std_logic;
  begin
    if (resetn = '0') then
      ready := '0';
    elsif (rising_edge(clk)) then
      -- Read data when ready and valid.
      if (st_valid = '1' and ready = '1') then
        ready := '0';
        -- Update compare
        comp  <= signed(st_data);
      end if;

      -- Sample compensation. Also prevents ready signals at reset conditions.
      if (counter_out + lead + 1 = top) then
        ready := '1';
      end if;
    end if;
    -- Signal ready to source
    st_ready <= ready;
  end process;

  -- Output duty cycle to compare as absolute
  comp_out <= abs(comp);
  -- Direction as the sign of the duty cycle
  dir      <= comp(DATA_WIDTH-1);
  -- Brake when the duty cycle value is zero
  brake    <= '1' when (comp = zero) else '0';
end architecture;
