library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm_dir is
  generic (BUS_WIDTH : integer := 16);

  port(rst, clk     : in  std_logic;
       brake        : in  std_logic;
       dir          : in  std_logic;
       ina, inb     : out std_ulogic;
       pwm          : out std_ulogic;
       compare, top : in  signed(BUS_WIDTH-1 downto 0);
       counter_out  : out signed(BUS_WIDTH-1 downto 0));
end pwm_dir;

architecture implementation of pwm_dir is
  subtype bit2 is std_ulogic_vector (1 downto 0);
  constant zeroes : signed(BUS_WIDTH-1 downto 0) := (others => '0');

  -- fast counter
  signal counter : signed(BUS_WIDTH-1 downto 0);

  -- Buffered signals, these are updated at counter = zero
  -- Updated on fast clock
  signal buf_compare, buf_top : signed(BUS_WIDTH-1 downto 0);
  signal buf_countmode        : std_logic;
  signal buf_dir              : std_logic;

  component dir_ctl is
    port(brake    : in  std_logic;
         dir      : in  std_logic;
         ina, inb : out std_ulogic);
  end component;

begin
  dir_c : dir_ctl
    port map(
      brake,
      buf_dir,
      ina,
      inb);

  process(rst, clk)
    variable tempcounter : signed(BUS_WIDTH-1 downto 0);
  begin
    tempcounter := (others => '0');
    -- reset buffered variables
    if rst = '0' then
      counter       <= (others => '0');
      buf_compare   <= (others => '0');
      buf_top       <= (others => '0');
      buf_countmode <= '0';
      buf_dir       <= '0';
    elsif rising_edge(clk) then
      if counter = buf_top then
        tempcounter := zeroes;
      else
        tempcounter := counter + 1;
      end if;
      -- Check if next value is zero and update buffers
      if tempcounter = zeroes then
        buf_top     <= top;
        buf_compare <= compare;
        buf_dir     <= dir;
      end if;
      counter <= tempcounter;
    end if;
  end process;
  pwm         <= '1' when counter < buf_compare else '0';
  counter_out <= counter;
end architecture;
