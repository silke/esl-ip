library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity quad_dec is
  generic (
    DATA_WIDTH : natural := 32  -- word size of each input and output register
    );
  port (
    -- signals to connect to an Avalon clock source interface
    clk    : in std_logic;
    resetn : in std_logic;

    -- signals to connect to an Avalon-MM slave interface
    slave_address    : in  std_logic_vector(2 downto 0);
    slave_read       : in  std_logic;
    slave_write      : in  std_logic;
    slave_readdata   : out std_logic_vector(DATA_WIDTH-1 downto 0);
    slave_writedata  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    slave_byteenable : in  std_logic_vector((DATA_WIDTH/8)-1 downto 0);

    irq_line : out std_logic;

    -- signals to connect to custom user logic
    quadrature_in : in std_logic_vector(1 downto 0)
    );
end entity;

architecture implementation of quad_dec is
  signal quad_count, quad_new : signed(DATA_WIDTH-1 downto 0);
  signal write_en, changed    : std_logic;

  -- Definition of the quadrature decoder
  component quad_decoder
    generic (
      BUS_WIDTH : natural := 8
      );
    port(
      rst, clk       : in  std_logic;
      phase1, phase2 : in  std_ulogic;
      quad_out       : out signed(BUS_WIDTH downto 1);
      quad_in        : in  signed(BUS_WIDTH downto 1);
      write_en       : in  std_logic;
      changed        : out std_logic
      );
  end component;

begin
  -- Initialization of the example
  qd : quad_decoder
    generic map(
      BUS_WIDTH => DATA_WIDTH
      )
    port map(resetn,
             clk,
             quadrature_in(0),
             quadrature_in(1),
             quad_count,
             quad_new,
             write_en,
             changed);

  -- Communication with the bus
  p_avalon : process(clk, resetn)
  begin
    if (resetn = '0') then
      irq_line <= '0';
      write_en <= '0';
    elsif (rising_edge(clk)) then
      quad_new <= (others => '0');
      write_en <= '0';
      if (slave_read = '1') then
        if slave_address = "000" then
          slave_readdata <= std_logic_vector(quad_count);
          irq_line       <= '0';
        end if;
      end if;
      if (slave_write = '1') then
        if slave_address = "000" then
          quad_new <= signed(slave_writedata);
          write_en <= '1';
        end if;
      end if;
      if changed = '1' then
        irq_line <= '1';
      end if;
    end if;
  end process;
end architecture;
