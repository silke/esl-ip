library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity quad_decoder is
  generic (BUS_WIDTH : integer := 16);

  port(rst, clk       : in  std_logic;
       phase1, phase2 : in  std_ulogic;
       quad_out       : out signed(BUS_WIDTH downto 1);
       quad_in        : in  signed(BUS_WIDTH downto 1);
       write_en       : in  std_logic;
       changed        : out std_logic);
end quad_decoder;

architecture implementation of quad_decoder is
  subtype bit2 is std_ulogic_vector (1 downto 0);
  constant low        : bit2 := "00";
  constant right_high : bit2 := "01";
  constant high       : bit2 := "11";
  constant left_high  : bit2 := "10";
  signal state        : bit2;
begin
  process(rst, clk)
    type diff_t is (none, up, down);
    variable diff       : diff_t;
    variable newstate   : bit2;
    variable counter    : signed(BUS_WIDTH downto 1);
    variable newcounter : signed(BUS_WIDTH downto 1);
  begin
    if rst = '0' then
      counter := (others => '0');
      state   <= low;
      changed <= '0';
    elsif rising_edge(clk) then
      -- concatenate inputs
      newstate   := phase1 & phase2;
      diff       := none;
      newcounter := counter;
      changed    <= '0';

      -- select case and new state
      case state is
        when low =>
          case newstate is
            when right_high => diff := up;
            when left_high  => diff := down;
            when others =>
          end case;
        when right_high =>
          case newstate is
            when high => diff := up;
            when low  => diff := down;
            when others =>
          end case;
        when high =>
          case newstate is
            when left_high  => diff := up;
            when right_high => diff := down;
            when others =>
          end case;
        when left_high =>
          case newstate is
            when low  => diff := up;
            when high => diff := down;
            when others =>
          end case;
        when others => assert false report "invalid state in encoder";
      end case;

      -- Update counter to input if write enable
      if write_en = '1' then
        newcounter := quad_in;
      end if;

      -- increment or decrement counter
      case diff is
        when up =>
          counter := newcounter + 1;
          changed <= '1';
        when down =>
          counter := newcounter - 1;
          changed <= '1';
        when others => counter := newcounter;
      end case;
      state <= newstate;
    end if;

    quad_out <= counter;
  end process;
end architecture;
