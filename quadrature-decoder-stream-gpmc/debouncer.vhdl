library ieee;
use ieee.std_logic_1164.all;

entity debouncer is
  port (
    clk    : in  std_logic;
    reset  : in  std_logic;
    input  : in  std_logic_vector(1 downto 0);
    output : out std_logic_vector(1 downto 0));
end debouncer;

architecture implementation of debouncer is
  signal dbrega, dbregb : std_logic_vector(7 downto 0);
  signal outa, outb     : std_logic;
  type samplestate_t is (sample, count);
  signal state          : samplestate_t;
begin

  process(clk, reset)
    variable counter : integer;
    constant delay   : integer := 1000;
  begin
    if reset = '0' then
      outa    <= input(0);
      outb    <= input(1);
      dbrega  <= (others => input(0));
      dbregb  <= (others => input(1));
      state   <= count;
      counter := delay;
    elsif rising_edge(clk) then
      case state is
        when sample =>
          dbrega  <= dbrega(6 downto 0) & input(0);
          dbregb  <= dbregb(6 downto 0) & input(1);
          counter := delay;
          state   <= count;
        when count =>
          counter := counter - 1;
          if counter = 0 then
            state <= sample;
          end if;
      end case;
      if dbrega = X"FF" then
        outa <= '1';
      elsif dbrega = X"00" then
        outa <= '0';
      end if;
      if dbregb = X"FF" then
        outb <= '1';
      elsif dbregb = X"00" then
        outb <= '0';
      end if;
    end if;
  end process;

  output <= outb & outa;
end implementation;



