library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity quad_dec is
  generic (
    DATA_WIDTH : natural := 32  -- word size of each input and output register
    );
  port (
    -- signals to connect to an Avalon clock source interface
    clk    : in std_logic;
    resetn : in std_logic;

    quad_out : out signed(DATA_WIDTH-1 downto 0);
    quad_in  : in  signed(DATA_WIDTH-1 downto 0);
    write    : in  std_logic;

    -- signals to connect to an Avalon-ST interface
    st_data  : out std_logic_vector(DATA_WIDTH-1 downto 0);
    st_ready : in  std_logic;
    st_valid : out std_logic;

    -- signals to connect to custom user logic
    quadrature_in : in std_logic_vector(1 downto 0)
    );
end entity;

architecture implementation of quad_dec is
  signal quad_count, quad_new : signed(DATA_WIDTH-1 downto 0);
  signal write_en, changed    : std_logic;

  constant zero : signed(DATA_WIDTH-1 downto 0) := (others => '0');

  -- Definition of the quadrature decoder
  component quad_decoder
    generic (
      BUS_WIDTH : natural := 32
      );
    port(
      rst, clk       : in  std_logic;
      phase1, phase2 : in  std_ulogic;
      quad_out       : out signed(BUS_WIDTH downto 1);
      quad_in        : in  signed(BUS_WIDTH downto 1);
      write_en       : in  std_logic;
      changed        : out std_logic
      );
  end component;

begin
  -- Initialization of the example
  qd : quad_decoder
    generic map(
      BUS_WIDTH => DATA_WIDTH
      )
    port map(resetn,
             clk,
             quadrature_in(0),
             quadrature_in(1),
             quad_count,
             quad_new,
             write_en,
             changed);

  -- Avalon streaming interface, source
  -- https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/manual/mnl_avalon_spec.pdf
  p_avalon_st : process(clk, resetn)
  begin
    if (resetn = '0') then
      st_valid <= '0';
      st_data  <= std_logic_vector(zero);
    elsif (rising_edge(clk)) then
      -- Default inactive outputs
      st_valid <= '0';
      st_data  <= std_logic_vector(zero);

      -- Wait for sink to signal ready and transmit data.
      if st_ready = '1' then
        st_valid <= '1';
        st_data  <= std_logic_vector(quad_count);
      end if;
    end if;
  end process;

  -- Port map values from GPMC interface
  quad_new <= quad_in;
  write_en <= write;
  quad_out <= quad_count;
end architecture;
