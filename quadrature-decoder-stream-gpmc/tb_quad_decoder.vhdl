library vunit_lib;
library ieee;
library osvvm;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use osvvm.RandomPkg.all;

entity tb_quad_decoder is
  generic (runner_cfg : string  := "";
           data_width : natural := 32;
           num_tests  : natural := 100);
end tb_quad_decoder;

architecture tester of tb_quad_decoder is

  component quad_dec is
    generic (DATA_WIDTH : integer);
    port(clk, resetn   : in  std_logic;
         quad_out      : out signed(DATA_WIDTH downto 1);
         quad_in       : in  signed(DATA_WIDTH downto 1);
         write         : in  std_ulogic;
         st_data       : out std_logic_vector(DATA_WIDTH-1 downto 0);
         st_ready      : in  std_logic;
         st_valid      : out std_logic;
         quadrature_in : in  std_ulogic_vector(1 downto 0));
  end component;

  component pwm_gpmc is
    generic (DATA_WIDTH : integer);
    port(
      clk    : in std_logic;
      resetn : in std_logic;

      top, lead : in signed(DATA_WIDTH-1 downto 0);


      -- signals to connect to an Avalon-ST interface
      st_data  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
      st_ready : out std_logic;
      st_valid : in  std_logic;

      -- signals to connect to custom user logic
      pwm_out : out std_logic;
      hba     : out std_logic;
      hbb     : out std_logic);
  end component;

  subtype bit2 is std_ulogic_vector (1 downto 0);

  constant bus_width  : integer                       := 32;
  constant ones       : signed(data_width-1 downto 0) := (others => '1');
  constant low        : bit2                          := "00";
  constant right_high : bit2                          := "01";
  constant high       : bit2                          := "11";
  constant left_high  : bit2                          := "10";

  signal state                     : bit2                          := low;
  signal clk, reset, write         : std_logic                     := '0';
  signal quad                      : signed(data_width-1 downto 0);
  signal newquad                   : signed(data_width-1 downto 0) := (others => '0');
  signal st_ready_in, st_valid_out : std_logic;
  signal st_data_out               : std_logic_vector(data_width-1 downto 0);
  signal ha1, hb1, out1            : std_ulogic;
  signal lead, top                 : signed(data_width-1 downto 0) := to_signed(0, data_width);

begin
  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;
  qd : quad_dec
    generic map (DATA_WIDTH => data_width)
    port map(clk,
             reset,
             quad,
             newquad,
             write,
             st_data_out,
             st_ready_in,
             st_valid_out,
             state);
  pwm : pwm_gpmc
    generic map (DATA_WIDTH => data_width)
    port map(clk,
             reset,
             top,
             lead,
             st_data_out,
             st_ready_in,
             st_valid_out,
             out1,
             ha1, hb1
             );

  main : process
    variable RV0, RV1, RV2 : RandomPType;
    variable counter       : signed(data_width-1 downto 0);
    variable change        : integer range -1 to 1;
    variable loopcount     : integer;
    variable looptime      : time;

    -- Increment the quadrature state
    procedure decrement is
    begin
      case state is
        when low        => state <= right_high;
        when right_high => state <= high;
        when high       => state <= left_high;
        when left_high  => state <= low;
        when others =>
      end case;
    end procedure;

    -- decrement the quadrature state
    procedure increment is
    begin
      case state is
        when low        => state <= left_high;
        when right_high => state <= low;
        when high       => state <= right_high;
        when left_high  => state <= high;
        when others =>
      end case;
    end procedure;

  begin
    -- Simulation start
    test_runner_setup(runner, runner_cfg);

    -- Seed the random values
    RV0.InitSeed (RV0'instance_name);
    RV1.InitSeed (RV1'instance_name);
    RV2.InitSeed (RV2'instance_name);

    -- Wait until the reset is done
    wait for 100 ns;

    -- Set new value and wait for update tick
    lead    <= to_signed(7, st_data_out'length);
    top     <= to_signed(4000, top'length);
    newquad <= to_signed(-1000, newquad'length);
    write   <= '1';
    wait until clk = '1';
    write   <= '0';
    wait until clk = '0';
    wait until clk = '1';

    -- Check initialisation values
    check_equal(newquad, quad, "New value does not match set value", failure);

    -- Initialise counter
    counter := quad;

    -- Test the quad_decoder with 5000 random situations
    for i in 0 to num_tests loop
      -- Generate random values for the change (increment, decrement or nothing),
      -- number of changes and time that the loop takes
      change    := RV0.RandInt(-1, 1);
      loopcount := RV1.RandInt(1, 200);
      looptime  := RV2.RandTime(60 ns, 1000 ns);

      -- Perform a number of changes
      for I in 0 to loopcount loop
        -- Select the kind of change
        case change is
          when 0 =>
          when 1  => increment;
          when -1 => decrement;
        end case;

        -- Wait for the set interval
        wait for looptime;

        -- Check if the predicted value is the same as the decoded value
        check_equal(counter + change, quad, "New value does not match incremented value", failure);

        -- Set counter to decoded value
        counter := quad;
      end loop;
    end loop;

    -- Simulation ends here
    test_runner_cleanup(runner);
  end process;
end tester;
