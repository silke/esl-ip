library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pid is
  generic (DATA_WIDTH : natural := 32);

  port(rst, clk    : in  std_logic;
       enable      : in  std_logic;
       ready       : out std_logic;
       kp, ki, kd  : in  signed(DATA_WIDTH - 1 downto 0);
       setpoint    : in  signed(DATA_WIDTH - 1 downto 0);
       position    : in  signed(DATA_WIDTH - 1 downto 0);
       out_limit   : in  signed(DATA_WIDTH - 1 downto 0);
       out_counter : out signed(DATA_WIDTH - 1 downto 0);
       out_err     : out signed(DATA_WIDTH - 1 downto 0);
       output      : out signed(DATA_WIDTH - 1 downto 0)
       );
end pid;

architecture implementation of pid is
  type state_t is (start, calc, gain, sum, o_ready);
  subtype int_t is signed(DATA_WIDTH - 1 downto 0);

  constant zero : int_t := (others => '0');

  signal state          : state_t;
  signal e, d_e, i, p_e : int_t;
  signal up, ui, ud     : signed(2*DATA_WIDTH - 1 downto 0);
  signal n_out_limit    : int_t;


begin
  -- Always calculate negative of n_out_limit
  n_out_limit <= -out_limit;

  -- State control
  process(clk, rst)
    variable o       : int_t;
    variable o_long  : signed(2*DATA_WIDTH - 1 downto 0);
    variable counter : int_t;
  begin
    if rst = '0' then
      e       <= zero;
      p_e     <= zero;
      d_e     <= zero;
      i       <= zero;
      counter := zero;
      state   <= start;
    elsif rising_edge(clk) then
      case state is
        when start =>
          if enable = '1' then
            state   <= calc;
            e       <= setpoint - position;
            counter := counter + 1;
          else
            state <= start;
          end if;
        when calc =>
          state <= gain;
          -- Calculate integral and differential components
          i     <= i + e;
          d_e   <= e - p_e;
          p_e   <= e;
        when gain =>
          state <= sum;
          -- Calculate parameters with given gains
          up    <= kp * e;
          ui    <= ki * i;
          ud    <= kd * d_e;
        when sum =>
          state  <= o_ready;
          -- Calculate output
          o_long := up + ui + ud;
          o      := o_long(47 downto 16);
          -- Limit calculated output value
          if o > out_limit then
            o := out_limit;
          elsif o < n_out_limit then
            o := n_out_limit;
          end if;
                                        -- Set output
          output <= o;
        when o_ready =>
          state <= start;
      end case;
    end if;
    out_counter <= counter;
    out_err     <= e;
  end process;

  -- PID stages
  process(state)
  begin
    ready <= '0';
    case state is
      when start =>
      when calc =>
      when gain =>
      when sum =>
      when o_ready =>
        -- Set ready
        ready <= '1';
    end case;
  end process;
end implementation;

