library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pid_gpmc is
  generic (
    DATA_WIDTH : natural := 32  -- word size of each input and output register
    );
  port (
    -- signals to connect to an Avalon clock source interface
    clk    : in std_logic;
    resetn : in std_logic;

    -- Avalon Quadrature Encoder connections
    qe_data  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    qe_ready : out std_logic;
    qe_valid : in  std_logic;

    -- Avalon PWM connections
    pwm_data  : out std_logic_vector(DATA_WIDTH-1 downto 0);
    pwm_ready : in  std_logic;
    pwm_valid : out std_logic;

    -- PID connections
    kp, ki, kd  : in  signed(DATA_WIDTH - 1 downto 0);
    setpoint    : in  signed(DATA_WIDTH - 1 downto 0);
    out_limit   : in  signed(DATA_WIDTH - 1 downto 0);
    out_counter : out signed(DATA_WIDTH - 1 downto 0);
    out_err     : out signed(DATA_WIDTH - 1 downto 0)
    );
end entity;

architecture implementation of pid_gpmc is
  signal i, e_prev, pid_setpoint : signed(DATA_WIDTH-1 downto 0);
  signal pid_ready, pid_enable   : std_logic;
  signal pid_out                 : signed(DATA_WIDTH - 1 downto 0);

  constant zero : signed(DATA_WIDTH-1 downto 0) := (others => '0');

  component pid
    generic (DATA_WIDTH : natural := 32);
    port(rst, clk    : in  std_logic;
         enable      : in  std_logic;
         ready       : out std_logic;
         kp, ki, kd  : in  signed(DATA_WIDTH - 1 downto 0);
         setpoint    : in  signed(DATA_WIDTH - 1 downto 0);
         position    : in  signed(DATA_WIDTH - 1 downto 0);
         out_limit   : in  signed(DATA_WIDTH - 1 downto 0);
         output      : out signed(DATA_WIDTH - 1 downto 0);
         out_counter : out signed(DATA_WIDTH - 1 downto 0);
         out_err     : out signed(DATA_WIDTH - 1 downto 0)
         );
  end component;
begin
  pid_c : pid
    generic map(DATA_WIDTH => DATA_WIDTH)
    port map(
      resetn, clk,
      pid_enable,
      pid_ready,
      kp, ki, kd,
      setpoint,
      signed(qe_data),
      out_limit,
      pid_out,
      out_counter,
      out_err);

  pid_enable <= qe_valid;
  pwm_valid  <= pid_ready;
  pwm_data   <= std_logic_vector(pid_out);

  p_avalon_st : process(clk, resetn)
  begin
    if (resetn = '0') then
      qe_ready <= '0';
    elsif rising_edge(clk) then
      -- If PWM is ready expect data
      if (pwm_ready = '1') and (qe_valid = '0') then
        qe_ready <= '1';
      else
        qe_ready <= '0';
      end if;
    end if;
  end process;
end;
