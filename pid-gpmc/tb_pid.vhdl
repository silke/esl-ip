library ieee;
library vunit_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use vunit_lib.lang.all;
use vunit_lib.string_ops.all;
use vunit_lib.dictionary.all;
use vunit_lib.path.all;
use vunit_lib.log_types_pkg.all;
use vunit_lib.log_special_types_pkg.all;
use vunit_lib.log_pkg.all;
use vunit_lib.check_types_pkg.all;
use vunit_lib.check_special_types_pkg.all;
use vunit_lib.check_pkg.all;
use vunit_lib.run_types_pkg.all;
use vunit_lib.run_special_types_pkg.all;
use vunit_lib.run_base_pkg.all;
use vunit_lib.run_pkg.all;


entity tb_pid is
  generic (runner_cfg : string  := "";
           data_width : natural := 32;
           pwm_mode   : natural := 0);
end tb_pid;

architecture tester of tb_pid is
  component pid
    generic (DATA_WIDTH : natural := 32);
    port(rst, clk   : in  std_logic;
         enable     : in  std_logic;
         ready      : out std_logic;
         kp, ki, kd : in  signed(DATA_WIDTH - 1 downto 0);
         setpoint   : in  signed(DATA_WIDTH - 1 downto 0);
         position   : in  signed(DATA_WIDTH - 1 downto 0);
         out_limit  : in  signed(DATA_WIDTH - 1 downto 0);
         output     : out signed(DATA_WIDTH - 1 downto 0)
         );
  end component;

  signal clk, reset                  : std_logic                     := '0';
  signal kp, ki, kd, setpoint        : signed(data_width-1 downto 0) := to_signed(0, data_width);
  signal position, out_limit, output : signed(data_width-1 downto 0) := to_signed(0, data_width);
begin
  reset <= '0', '1' after 100 ns;
  clk   <= not clk  after 10 ns;

  pid_c : pid
    generic map(DATA_WIDTH => DATA_WIDTH)
    port map(
      reset, clk,
      enable, ready,
      kp, ki, kd,
      setpoint, position,
      out_limit, output);

  main : process
  begin
    -- Simulation start
    test_runner_setup(runner, runner_cfg);

    -- Set PID gains
    kp        <= to_signed(1, data_width);
    ki        <= to_signed(2, data_width);
    kd        <= to_signed(3, data_width);
    out_limit <= to_signed(50, data_width);

    -- Wait until the reset is done
    wait for 100 ns;

    -- Set setpoint/position
    setpoint <= to_signed(50, data_width);
    position <= to_signed(0, data_width);
    enable   <= '1';

    -- Wait for calculation
    wait until ready = '1';

    -- Check values
    check_equal(output, 0, "PID output");

    -- End of test
    test_runner_cleanup(runner);
  end process;
end tester;
