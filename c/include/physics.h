#pragma once

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

typedef struct {
  double x;
  double y;
  double z;
  double t;
} vector_t;

typedef struct {
  double psi;
  double theta;
} angle_t;

typedef struct {
  angle_t a[2];
  double t;
} angle_sample_t;

vector_t calc_position(double l, angle_sample_t s);
vector_t calc_position_with_time(double l, angle_t a[2], double t);
vector_t calc_velocity(vector_t p1, vector_t p2);
vector_t calc_acceleration(vector_t v1, vector_t v2);
vector_t calc_landing(double floor_z, vector_t p, vector_t v, vector_t a);
angle_t calc_angle(double x, double y, double x_max, double y_max, double phi_x,
                   double phi_y);
int angle_to_encoder(double a, int enc_max);
double encoder_to_angle(int e, int enc_max);
angle_t add_angle(angle_t a, angle_t b);
double diff_timeval(struct timeval t1, struct timeval t2);
