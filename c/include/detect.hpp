#pragma once

#include <iostream>
#include <stdlib.h>

#include <opencv2/opencv.hpp>

extern "C" {
#include "gpmc_driver.h"
#include "physics.h"
}

#define CAM_MAX_X 320
#define CAM_MAX_Y 240
#define MINRADIUS 5
#define CAM_PHI_X 1.571 // Rad, 90.0 degrees
#define CAM_PHI_Y 1.257 // Rad, 72.0 degrees

using namespace std;
using namespace cv;

class Detector {
protected:
  VideoCapture cap;
  Mat frame, frame_hsv, frame_threshold, frame_erode, kernel;

public:
  int low_r, low_g, low_b;
  int high_r, high_g, high_b;

  Detector(); // This does nothing!
  Detector(int device);
  void BrightnessAndContrastAuto(const cv::Mat &src, cv::Mat &dst,
                                 float clipHistPercent);
  int Detect(angle_t *s, int render);
};
