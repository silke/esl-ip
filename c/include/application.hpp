#pragma once

#include "detect.hpp"

#define VAR_START 0x24
#define SET_START 0x34

#define SETTINGS_START 0xC

#define ENC_MAX_X 5000
#define ENC_MAX_Y 2000

#define ENC_LIMIT_X 2000
#define ENC_LIMIT_Y 400

typedef struct {
  int32_t counter;
  int32_t encoder;
  int32_t error;
  int32_t pwm;
} measurement_t;

typedef struct {
  int32_t tilt;
  int32_t pan;
} setpoint_t;

typedef struct {
  int32_t kp;
  int32_t ki;
  int32_t kd;
} pid_settings_t;

typedef struct {
  int32_t limit;
  int32_t top;
  int32_t lead;
} pwm_settings_t;

typedef struct {
  pid_settings_t pid;
  pwm_settings_t pwm;
} controller_settings_t;

typedef struct { controller_settings_t tilt, pan; } cam_controller_settings_t;

class GPMC {
public:
  int fd;
  int measure_start;
  int setpoint_start;
  int settings_start;

  GPMC(const char *path);
  void ReadMeasurements(measurement_t *m, int n);
  void SetSetpoints(setpoint_t *s, int n);
  void Configure(cam_controller_settings_t *c, int n);
  void Close();
};

angle_t MeasurementToAngle(measurement_t m_pan, measurement_t m_tilt);

setpoint_t AngleToSetpoint(angle_t a);

int32_t tofixed(float x);
