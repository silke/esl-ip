#pragma once

#include <cstdlib>

class TCP {
public:
  int sockfd;

  int Read(char *b, size_t n);
  void Write(char *b, size_t n);
  void Close();
};

class TCPClient : public TCP {
public:
  TCPClient(char *address, int port);
};

class TCPServer : public TCP {
private:
  int lsockfd;

public:
  TCPServer(int port);

  void Accept();
  void CloseAll();
};
