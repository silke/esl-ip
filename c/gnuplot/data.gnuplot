# index
# time
# a(psi)
# a(theta)
# b(psi)
# b(theta)
# c(psi)
# c(theta)
# pan(count)
# pan(enc)
# pan(error)
# pan(pwm)
# tilt(count)
# tilt(enc)
# tilt(error)
# tilt(pwm)
# set(pan)
# set(tilt)

reset                                   # reset
set size ratio 0.2                      # set relative size of plots
set xlabel 'time (s)'                   # set x-axis label for all plots
set grid xtics ytics                    # grid: enable both x and y lines
set grid lt 1 lc rgb '#cccccc' lw 1     # grid: thin gray lines
set key outside horizontal bottom right
set multiplot layout 4,1 scale 1.0,1.0  # set three plots for this figure

## Angle values
set offsets 0,0,0.1,0.1

set title 'Ball location (pan)'
set ylabel 'angle (rad)'
plot file using 2:3 with lines lt 1 lw 2 title 'Camera',\
     file using 2:5 with lines lt 2 lw 2 title 'Motor',\
     file using 2:7 with lines lt 3 lw 2 title 'Target'`

set title 'Ball location (tilt)'
set ylabel 'tilt angle (rad)'
plot file using 2:4 with lines lt 1 lw 2 title 'Camera',\
     file using 2:6 with lines lt 2 lw 2 title 'Motor',\
     file using 2:8 with lines lt 3 lw 2 title 'Target'`


## Controller values
set offsets 0,0,50,50

set title 'Controller (pan)'
set ylabel 'encoder value'
plot file using 2:10 with lines lt 1 lw 2 title 'Value',\
     file using 2:17 with lines lt 2 lw 2 title 'Setpoint',\
     file using 2:11 with lines lt 3 lw 2 title 'Error'

set title 'Controller (tilt)'
set ylabel 'encoder value'
plot file using 2:14 with lines lt 1 lw 2 title 'Value',\
     file using 2:18 with lines lt 2 lw 2 title 'Setpoint',\
     file using 2:15 with lines lt 3 lw 2 title 'Error',

unset multiplot
