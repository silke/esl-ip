#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

#include "support.hpp"
#include "tcp.hpp"

void TCP::Write(char *b, size_t n) {
  int r = send(sockfd, b, n, 0);
  if (r < 0) {
    error("ERROR writing to socket");
  }
}

int TCP::Read(char *b, size_t n) {
  int c = recv(sockfd, b, n, 0);
  if (c < 0) {
    error("ERROR reading from socket");
  }

  return c;
}

void TCP::Close() { close(sockfd); }

TCPClient::TCPClient(char *address, int port) {
  struct sockaddr_in6 serv_addr;

  // Set address parameters
  serv_addr.sin6_flowinfo = 0;
  serv_addr.sin6_family = AF_INET6;
  serv_addr.sin6_port = htons(port);

  // Convert port & address
  inet_pton(AF_INET6, address, &(serv_addr.sin6_addr));

  // Open sockes
  sockfd = socket(AF_INET6, SOCK_STREAM, 0);
  if (sockfd < 0)
    error("ERROR opening socket");

  // Connect
  if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    error("ERROR connecting");
}

TCPServer::TCPServer(int port) {
  struct sockaddr_in6 serv_addr;

  // Open socket
  lsockfd = socket(AF_INET6, SOCK_STREAM, 0);
  if (lsockfd < 0)
    error("ERROR opening socket\n");

  // Configure address
  serv_addr.sin6_family = AF_INET6;
  serv_addr.sin6_addr = in6addr_any;
  serv_addr.sin6_port = htons(port);

  // Bind to specified address/port
  if (bind(lsockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    error("ERROR binding");

  // Listen
  listen(lsockfd, 5);
}

void TCPServer::Accept() {
  sockfd = accept(lsockfd, NULL, NULL);

  if (sockfd < 0)
    error("ERROR on accept");
}

void TCPServer::CloseAll() {
  Close();
  close(lsockfd);
}
