#include "support.hpp"

void error(std::string msg) {
  std::cerr << msg << std::endl;
  exit(1);
}
