/**
 * @file gpmc_driver_c.c
 * @brief Implementation file.
 * @author Jan Jaap Kempenaar, University of Twente.
 */

#include "gpmc_driver.h"

#include <fcntl.h>     // open()
#include <sys/ioctl.h> // ioctl()
#include <unistd.h>    // close()

#define GPMCDEV_MAJOR 247 /* From the local/experimental list */

struct gpmc_req_data {
  unsigned int offset; ///< Register offset value.
  unsigned int value;  ///< Data field.
};

struct gpmc_req_array {
  unsigned int offset; ///< Register offset value.
  unsigned int *data;  ///< Pointer to data array.
  unsigned int len;    ///< Length of the array.
};

#define IOCTL_SET_U16                                                          \
  _IOW(GPMCDEV_MAJOR, 0, struct gpmc_req_data) ///< 16-bit read action id
#define IOCTL_GET_U16                                                          \
  _IOWR(GPMCDEV_MAJOR, 1, struct gpmc_req_data) ///< 16-bit write action id
#define IOCTL_SET_U32                                                          \
  _IOW(GPMCDEV_MAJOR, 2, struct gpmc_req_data) ///< 32-bit read action id
#define IOCTL_GET_U32                                                          \
  _IOWR(GPMCDEV_MAJOR, 3, struct gpmc_req_data) ///< 32-bit write action id
#define GPMC_IOC_WR32_ARRAY                                                    \
  _IOW(GPMCDEV_MAJOR, 6, struct gpmc_req_array) ///< 32-bit read action id
#define GPMC_IOC_RD32_ARRAY                                                    \
  _IOWR(GPMCDEV_MAJOR, 7, struct gpmc_req_array) ///< 32-bit write action id

unsigned long getGPMCValue(int fd, int idx) {
  // Read from specified address.
  struct gpmc_req_data temp;
  temp.offset = idx;
  ioctl(fd, IOCTL_GET_U32, &temp);
  return temp.value;
}

void setGPMCValue(int fd, unsigned int value, int idx) {
  // create data structure and fill with data.
  struct gpmc_req_data temp;
  temp.value = value;
  temp.offset = idx;
  // Set value.
  ioctl(fd, IOCTL_SET_U32, &temp);
}

void setGPMCArray(int fd, int idx, unsigned int *value, unsigned int len) {
  struct gpmc_req_array temp;
  temp.data = value;
  temp.len = len;
  temp.offset = idx;
  ioctl(fd, GPMC_IOC_WR32_ARRAY, &temp);
}

void getGPMCArray(int fd, int idx, unsigned int *value, unsigned int len) {
  struct gpmc_req_array temp;
  temp.data = value;
  temp.len = len;
  temp.offset = idx;
  ioctl(fd, GPMC_IOC_RD32_ARRAY, &temp);
}
