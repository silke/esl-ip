#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include <fcntl.h>  // open()
#include <unistd.h> // close()

#include "application.hpp"

GPMC::GPMC(const char *path) {
  fd = open(path, 0);

  if (0 > fd) {
    printf("Error, could not open device: %s.\n", path);
    exit(1);
  }

  measure_start = VAR_START;
  setpoint_start = SET_START;
  settings_start = SETTINGS_START;
}

void GPMC::ReadMeasurements(measurement_t *m, int n) {
  getGPMCArray(fd, sizeof(uint32_t) * measure_start, (unsigned int *)m,
               n * sizeof(measurement_t));
}

void GPMC::SetSetpoints(setpoint_t *s, int n) {
  setGPMCArray(fd, sizeof(uint32_t) * setpoint_start, (unsigned int *)s,
               n * sizeof(setpoint_t));
}

void GPMC::Configure(cam_controller_settings_t *s, int n) {
  setGPMCArray(fd, sizeof(uint32_t) * settings_start, (unsigned int *)s,
               n * sizeof(cam_controller_settings_t));
}

void GPMC::Close() { close(fd); }

angle_t MeasurementToAngle(measurement_t m_pan, measurement_t m_tilt) {
  angle_t a;

  a.psi = encoder_to_angle(m_pan.encoder, ENC_MAX_X);
  a.theta = encoder_to_angle(m_tilt.encoder, ENC_MAX_Y);

  return a;
}

setpoint_t AngleToSetpoint(angle_t a) {
  setpoint_t s;

  s.pan = angle_to_encoder(a.psi, ENC_MAX_X);
  s.tilt = angle_to_encoder(a.theta, ENC_MAX_Y);

  if (s.pan > ENC_LIMIT_X)
    s.pan = ENC_LIMIT_X;

  if (s.pan < -ENC_LIMIT_X)
    s.pan = -ENC_LIMIT_X;

  if (s.tilt > ENC_LIMIT_Y)
    s.tilt = ENC_LIMIT_Y;

  if (s.tilt < -ENC_LIMIT_Y)
    s.tilt = -ENC_LIMIT_Y;

  return s;
}

int32_t tofixed(float x) { return (int32_t)(x * powf(2.0, 16.0)); }
