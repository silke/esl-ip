#include "detect.hpp"

#define DEBUG 0

Detector::Detector() {}

Detector::Detector(int device) {
  // RGB settings
  low_r = 0, low_g = 182, low_b = 183;
  high_r = 105, high_g = 255, high_b = 255;

  // HSV settings
  // low_r = 78, low_g = 167, low_b = 143;
  // high_r = 101, high_g = 255, high_b = 255;

  VideoCapture c(device);
  c.set(CV_CAP_PROP_FRAME_WIDTH, 320);
  c.set(CV_CAP_PROP_FRAME_HEIGHT, 240);

  cap = c;

  kernel = getStructuringElement(MORPH_RECT, Size(3 * 3 + 1, 3 * 3 + 1),
                                 Point(3, 3));
}

int Detector::Detect(angle_t *s, int render) {
  vector<KeyPoint> keypoints;
  vector<KeyPoint> renderpoint;
  cv::vector<cv::vector<cv::Point>> contours;
  cv::vector<cv::Vec4i> hierarchy;
  int maxradius = 0;
  Point2i maxcenter;
  // Retrieve frame
  cap >> frame;

  // Return an error if the frame is empty
  if (frame.empty())
    return -1;

  // Convert to HSV
  // cvtColor(frame, frame_hsv, CV_RGB2HSV);

  // Detect the object based on RGB Range Values
  inRange(frame, Scalar(low_r, low_g, low_b), Scalar(high_r, high_g, high_b),
          frame_erode);

  cv::findContours(frame_erode.clone(), contours, hierarchy, CV_RETR_TREE,
                   CV_CHAIN_APPROX_NONE);

  size_t count = contours.size();
  size_t pointcount = 0;

  for (int i = 0; i < count; i++) {
    cv::Point2f c;
    float r;
    cv::minEnclosingCircle(contours[i], c, r);
    if (r > MINRADIUS && r > maxradius) {
      maxcenter = c;
      maxradius = r;
      pointcount++;
    }
  }
  if (pointcount > 0) {
    // Calculate the angle
    *s = calc_angle((double)maxcenter.x, (double)maxcenter.y, CAM_MAX_X,
                    CAM_MAX_Y, CAM_PHI_X, CAM_PHI_Y);

    // Debug output
    if (DEBUG) {
      std::cout << "largest point found at " << maxcenter << " with radius "
                << maxradius << "." << std::endl;
    }

    // Render the point
    if (render) {
      cv::circle(frame, maxcenter, maxradius, Scalar(0, 0, 255), 3);
    }

    return maxradius;
  }
  return 0;
}
