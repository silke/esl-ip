#include "physics.h"

vector_t calc_position(double l, angle_sample_t s) {
  return calc_position_with_time(l, s.a, s.t);
}

vector_t calc_position_with_time(double l, angle_t a[2], double t) {
  double h1;
  vector_t pos;

  h1 = l * cos(a[1].psi) / sin(a[0].psi - a[1].psi);

  pos.x = h1 * sin(a[0].psi);
  pos.y = h1 * cos(a[0].psi);
  pos.z = h1 * tan(a[0].theta);
  pos.t = t;

  return pos;
}

vector_t calc_velocity(vector_t p1, vector_t p2) {
  vector_t v;

  v.t = p2.t - p1.t;

  if (v.t < 0) {
    v.t += 1;
  }

  v.x = (p2.x - p1.x) / v.t;
  v.y = (p2.y - p1.y) / v.t;
  v.z = (p2.z - p1.z) / v.t;

  return v;
}

vector_t calc_acceleration(vector_t v1, vector_t v2) {
  vector_t a;

  a.t = 0.5 * (v1.t + v2.t);
  a.x = (v2.x - v1.x) / a.t;
  a.y = (v2.y - v1.y) / a.t;
  a.z = (v2.z - v1.z) / a.t;

  return a;
}

vector_t calc_landing(double floor_z, vector_t p, vector_t v, vector_t a) {
  vector_t l;

  // Landing height
  l.z = floor_z;

  // Calculate time to landing with acceleration
  l.t = (-v.z - sqrt(powf(v.z, 2) - 2 * a.z * (p.z - l.z))) / a.z;

  if (l.t < 0) {
    return l;
  }

  // l.t = sqrt(2 * (l.z - p.z) / a.z);

  // Extrapolate x/y from velocity
  l.x = p.x + v.x * l.t + .5 * a.x * powf(l.t, 2);
  l.y = p.y + v.y * l.t + .5 * a.y * powf(l.t, 2);

  return l;
};

angle_t calc_angle(double x, double y, double x_max, double y_max, double phi_x,
                   double phi_y) {
  angle_t a;

  a.psi = (x / x_max - .5) * phi_x;
  a.theta = (y / y_max - .5) * -phi_y;

  return a;
};

int angle_to_encoder(double a, int enc_max) {
  return (int)(a / (2 * M_PI) * enc_max);
}

double encoder_to_angle(int e, int enc_max) {
  return (double)e / (double)enc_max * 2 * M_PI;
}

angle_t add_angle(angle_t a, angle_t b) {
  angle_t c;

  c.psi = a.psi + b.psi;
  c.theta = a.theta + b.theta;

  return c;
}

double diff_timeval(struct timeval t1, struct timeval t2) {
  double dt = (t2.tv_sec - t1.tv_sec);
  double du = (t2.tv_usec - t1.tv_usec) / 1000000.0;

  return dt + du;
}
