#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <thread>

#include "application.hpp"
#include "support.hpp"
#include "tcp.hpp"

#define GPMC_DEV "/dev/gpmcdev0"
#define GPMC_ENABLE

#define SW 12
#define SD 4
#define SAMPLES 20000
#define STEPPOINT M_PI / 3.0
#define SAMPLEDELAY 100

typedef struct {
  angle_t a, b, c;
  double t;
  measurement_t tilt;
  measurement_t pan;
  setpoint_t s;
} transfer_t;

void source(char *host, int port, cam_controller_settings_t *s) {
  int32_t prev_pan = 0;
  transfer_t t;
  struct timeval t1, t2;

  // Create and open socket
  TCPClient socket(host, port);

#ifdef GPMC_ENABLE
  // Initialise object
  GPMC gpmc(GPMC_DEV);

  // Write settings
  gpmc.Configure(s, 1);
#endif

  // Set the initial time
  gettimeofday(&t1, NULL);

  // Run control loop
  for (int i = 0; i < SAMPLES; i++) {
    // Pan step
    if (i > SAMPLES / 4 && i < SAMPLES / 2) {
      t.c.psi = STEPPOINT;
    } else {
      t.c.psi = -STEPPOINT;
    }

    // Tilt step
    if (i > SAMPLES / 2 && i < 3 * SAMPLES / 4) {
      t.c.theta = STEPPOINT;
    } else {
      t.c.theta = -STEPPOINT;
    }

    // Set the current time
    gettimeofday(&t2, NULL);

#ifdef GPMC_ENABLE
    // Read measurements
    gpmc.ReadMeasurements(&t.tilt, 2);
#endif

    // Convert angles
    t.b = MeasurementToAngle(t.pan, t.tilt);
    t.s = AngleToSetpoint(t.c);
    t.t = diff_timeval(t1, t2);

#ifdef GPMC_ENABLE
    // Set setpoint
    gpmc.SetSetpoints(&t.s, 1);
#endif

    // Write to network if the value is different
    if (t.pan.counter > prev_pan) {
      prev_pan = t.pan.counter;
      socket.Write((char *)&t, sizeof(transfer_t));
    }

    // Delay
    std::this_thread::sleep_for(std::chrono::microseconds(SAMPLEDELAY));
  }

  socket.Close();
#ifdef GPMC_ENABLE
  gpmc.Close();
#endif
}

void sink(int port) {
  std::ofstream f;
  transfer_t t;
  int32_t prev_pan = 0;
  int32_t prev_tilt = 0;
  int32_t offset_pan = 0;
  int32_t offset_tilt = 0;
  int32_t pan_error = 0;
  int32_t tilt_error = 0;
  unsigned int i = 0;

  f.open("data/net-step.dat");

  f << std::right << std::fixed << std::setprecision(SD);

  f << "# " << std::setw(SW - 2) << "index" << std::setw(SW) << "time"
    << std::setw(SW) << "a(psi)" << std::setw(SW) << "a(theta)" << std::setw(SW)
    << "b(psi)" << std::setw(SW) << "b(theta)" << std::setw(SW) << "c(psi)"
    << std::setw(SW) << "c(theta)" << std::setw(SW) << "pan(count)"
    << std::setw(SW) << "pan(enc)" << std::setw(SW) << "pan(error)"
    << std::setw(SW) << "pan(pwm)" << std::setw(SW) << "tilt(count)"
    << std::setw(SW) << "tilt(enc)" << std::setw(SW) << "tilt(error)"
    << std::setw(SW) << "tilt(pwm)" << std::setw(SW) << "set(pan)"
    << std::setw(SW) << "set(tilt)" << std::endl;

  // Open socket
  TCPServer socket(port);
  socket.Accept();

  while (true) {
    // Read data from socket
    if (socket.Read((char *)&t, sizeof(transfer_t)) == 0)
      break;

    // Get offset if counter is zero
    if (offset_pan == 0) {
      offset_pan = t.pan.counter;
    }

    // Get offset if counter is zero
    if (offset_tilt == 0) {
      offset_tilt = t.tilt.counter;
    }

    // Don't write invalid values (counter goes down)
    if (t.pan.counter < prev_pan || t.tilt.counter < prev_tilt) {
      continue;
    }

    // Save values
    prev_pan = t.pan.counter;
    prev_tilt = t.tilt.counter;
    pan_error += abs(t.pan.error);
    tilt_error += abs(t.tilt.error);

    f << std::setw(SW) << i << std::setw(SW) << t.t << std::setw(SW) << t.a.psi
      << std::setw(SW) << t.a.theta << std::setw(SW) << t.b.psi << std::setw(SW)
      << t.b.theta << std::setw(SW) << t.c.psi << std::setw(SW) << t.c.theta
      << std::setw(SW) << t.pan.counter - offset_pan << std::setw(SW)
      << t.pan.encoder << std::setw(SW) << t.pan.error << std::setw(SW)
      << t.pan.pwm << std::setw(SW) << t.tilt.counter - offset_tilt
      << std::setw(SW) << t.tilt.encoder << std::setw(SW) << t.tilt.error
      << std::setw(SW) << t.tilt.pwm << std::setw(SW) << t.s.pan
      << std::setw(SW) << t.s.tilt << std::endl;

    i++;
  }

  std::cout << "Total error:" << std::endl
            << " pan: " << pan_error << std::endl
            << " tilt: " << tilt_error << std::endl;

  socket.CloseAll();
  f.close();
}

int main(int argc, char *argv[]) {
  cam_controller_settings_t s;

  // Predefined controller values
  s.pan.pwm.limit = 2500;
  s.pan.pwm.top = 2500;
  s.pan.pwm.lead = 10;
  s.tilt.pwm.limit = 2500;
  s.tilt.pwm.top = 2500;
  s.tilt.pwm.lead = 10;

  if (argc == 2) {
    // Run program
    sink(atoi(argv[1]));

  } else if (argc == 9) {
    // Controller settings
    s.pan.pid.kp = tofixed(atof(argv[3]));
    s.pan.pid.ki = tofixed(atof(argv[4]));
    s.pan.pid.kd = tofixed(atof(argv[5]));
    s.tilt.pid.kp = tofixed(atof(argv[6]));
    s.tilt.pid.ki = tofixed(atof(argv[7]));
    s.tilt.pid.kd = tofixed(atof(argv[8]));

    // Run program
    source(argv[1], atoi(argv[2]), &s);

  } else {
    // Show usage message
    std::cerr << "Usage: " << argv[0]
              << " [address] port [pkp pki pkd tkp tki tkd]" << std::endl;
    exit(0);
  }

  return 0;
}
