#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "application.hpp"
#include "support.hpp"
#include "tcp.hpp"

#define GPMC_DEV "/dev/gpmcdev0"
#define GPMC_ENABLE

#define SW 12
#define SD 4

typedef struct {
  angle_t a, b, c;
  double t;
  measurement_t tilt;
  measurement_t pan;
  setpoint_t s;
} transfer_t;

void source(char *host, int port) {
  int e;
  transfer_t t;
  struct timeval t1, t2;

  // Create and open socket
  TCPClient socket(host, port);

// Initialise object
#ifdef GPMC_ENABLE
  GPMC gpmc(GPMC_DEV);
#endif

  // Initialise camera
  Detector cam(0);

  // Set the initial time
  gettimeofday(&t1, NULL);

  // Run control loop
  while (char(waitKey(1)) != 'q') {
    // Detect frames
    e = cam.Detect(&t.a, 0);
    if (e < 0) {
      error("Error while detecting frame");
    }
    if (e == 0) {
      continue;
    }

    // Set the initial time
    gettimeofday(&t2, NULL);

// Read measurements
#ifdef GPMC_ENABLE
    gpmc.ReadMeasurements(&t.tilt, 2);
#endif

    // Convert angles
    t.b = MeasurementToAngle(t.pan, t.tilt);
    t.c = add_angle(t.a, t.b);
    t.s = AngleToSetpoint(t.c);
    t.t = diff_timeval(t1, t2);

// Set setpoint
#ifdef GPMC_ENABLE
    gpmc.SetSetpoints(&t.s, 1);
#endif

    // Write to network
    socket.Write((char *)&t, sizeof(transfer_t));
  }

  socket.Close();
#ifdef GPMC_ENABLE
  gpmc.Close();
#endif
}

void sink(int port) {
  std::ofstream f;
  transfer_t t;
  unsigned int i = 0;
  f.open("data/net-data.dat");

  f << std::right << std::fixed << std::setprecision(SD);

  f << "# " << std::setw(SW - 2) << "index" << std::setw(SW) << "time"
    << std::setw(SW) << "a(psi)" << std::setw(SW) << "a(theta)" << std::setw(SW)
    << "b(psi)" << std::setw(SW) << "b(theta)" << std::setw(SW) << "c(psi)"
    << std::setw(SW) << "c(theta)" << std::setw(SW) << "pan(count)"
    << std::setw(SW) << "pan(enc)" << std::setw(SW) << "pan(error)"
    << std::setw(SW) << "pan(pwm)" << std::setw(SW) << "tilt(count)"
    << std::setw(SW) << "tilt(enc)" << std::setw(SW) << "tilt(error)"
    << std::setw(SW) << "tilt(pwm)" << std::setw(SW) << "set(pan)"
    << std::setw(SW) << "set(tilt)" << std::endl;

  // Open socket
  TCPServer socket(port);
  socket.Accept();

  while (true) {
    // Read angle from socket
    if (socket.Read((char *)&t, sizeof(transfer_t)) == 0)
      break;

    f << std::setw(SW) << i << std::setw(SW) << t.t << std::setw(SW) << t.a.psi
      << std::setw(SW) << t.a.theta << std::setw(SW) << t.b.psi << std::setw(SW)
      << t.b.theta << std::setw(SW) << t.c.psi << std::setw(SW) << t.c.theta
      << std::setw(SW) << t.pan.counter << std::setw(SW) << t.pan.encoder
      << std::setw(SW) << t.pan.error << std::setw(SW) << t.pan.pwm
      << std::setw(SW) << t.tilt.counter << std::setw(SW) << t.tilt.encoder
      << std::setw(SW) << t.tilt.error << std::setw(SW) << t.tilt.pwm
      << std::setw(SW) << t.s.pan << std::setw(SW) << t.s.tilt << std::endl;

    i++;
  }

  socket.CloseAll();
  f.close();
}

int main(int argc, char *argv[]) {
  if (argc == 2) {
    sink(atoi(argv[1]));
  } else if (argc == 3) {
    source(argv[1], atoi(argv[2]));
  } else {
    std::cerr << "Usage: " << argv[0] << " [address] port" << std::endl;
    exit(0);
  }

  return 0;
}
