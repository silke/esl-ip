#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "application.hpp"
#include "support.hpp"

#define DEBUG 1

#define GPMC_DEV "/dev/gpmcdev0"

int main() {
  int e;
  angle_t a, b, c;
  measurement_t measure[2];
  setpoint_t set;
  struct timeval t1, t2;

  // Initialise object
  GPMC gpmc(GPMC_DEV);

  // Initialise camera
  Detector d(0);

  // Run control loop
  while (char(waitKey(1)) != 'q') {
    // Get start time
    gettimeofday(&t1, NULL);

    // Detect frames
    e = d.Detect(&a, 0);
    if (e < 0) {
      error("Error while detecting frame");
    }
    if (e == 0) {
      continue;
    }

    // Stop time
    gettimeofday(&t2, NULL);

    // Read measurements
    gpmc.ReadMeasurements(measure, 2);

    // Convert angles
    b = MeasurementToAngle(measure[1], measure[0]);
    c = add_angle(a, b);
    set = AngleToSetpoint(c);

    // Set setpoint
    gpmc.SetSetpoints(&set, 1);

    if (DEBUG) {
      printf("Tilt measurement %i: encoder at: %i, error: %i, output: %i\n",
             measure[0].counter, measure[0].encoder, measure[0].error,
             measure[0].pwm);

      printf("Pan measurement %i: encoder at: %i, error: %i, output: %i\n",
             measure[1].counter, measure[1].encoder, measure[1].error,
             measure[1].pwm);

      printf("Ball angles: (%.2f, %.2f) = (%.2f, %.2f) + (%.2f, %.2f) \n",
             c.psi, c.theta, a.psi, a.theta, b.psi, b.theta);

      printf("Setpoints: pan %i, tilt %i\n", set.pan, set.tilt);

      printf("Time taken: %.4f\n", diff_timeval(t1, t2));
    }
  }

  return 0;
}
