#include "gpmc_driver.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>  // open()
#include <unistd.h> // close()
#define GPMC "/dev/gpmcdev0"
#define VAR_START 0x1F

#define ENC_MAX_X 5000
#define ENC_MAX_Y 2000

typedef struct {
  int32_t counter;
  int32_t encoder;
  int32_t error;
  int32_t pwm;
} measurement_t;

int main() {
  int fd; // File descriptor.
  measurement_t measure[2];
  fd = open(GPMC, 0);
  if (0 > fd) {
    printf("Error, could not open device: %s.\n", GPMC);
    return 1;
  }
  while (1) {

    getGPMCArray(fd, VAR_START, (unsigned int *)&measure,
                 sizeof(measure) / sizeof(int32_t));
    printf("Tilt measurement %i: encoder at: %i, error: %i, output: %i\n",
           measure[0].counter, measure[0].encoder, measure[0].error,
           measure[0].pwm);
    printf("Pan measurement %i: encoder at: %i, error: %i, output: %i\n",
           measure[1].counter, measure[1].encoder, measure[1].error,
           measure[1].pwm);
  }
}
