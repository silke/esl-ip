#include <sys/time.h>

#include "detect.hpp"

void on_low_r_thresh_trackbar(int, void *);
void on_righ_r_thresh_trackbar(int, void *);
void on_low_g_thresh_trackbar(int, void *);
void on_righ_g_thresh_trackbar(int, void *);
void on_low_b_thresh_trackbar(int, void *);
void on_righ_b_thresh_trackbar(int, void *);

class GUIDetector : public Detector {
public:
  GUIDetector(int device) : Detector(device) {}

  // Trackbars to set thresholds for RGB values
  void CreateGUI() {
    namedWindow("Video Capture", WINDOW_NORMAL);
    namedWindow("Object Detection", WINDOW_NORMAL);

    createTrackbar("Low H", "Object Detection", &low_r, 255,
                   on_low_r_thresh_trackbar);
    createTrackbar("High H", "Object Detection", &high_r, 255,
                   on_righ_r_thresh_trackbar);
    createTrackbar("Low S", "Object Detection", &low_g, 255,
                   on_low_g_thresh_trackbar);
    createTrackbar("High S", "Object Detection", &high_g, 255,
                   on_righ_g_thresh_trackbar);
    createTrackbar("Low V", "Object Detection", &low_b, 255,
                   on_low_b_thresh_trackbar);
    createTrackbar("High V", "Object Detection", &high_b, 255,
                   on_righ_b_thresh_trackbar);
  }

  void ShowFrames() {
    imshow("Video Capture", frame);
    imshow("Threshold", frame_erode);
  }
};

GUIDetector d(1);

void on_low_r_thresh_trackbar(int, void *) {
  d.low_r = min(d.high_r - 1, d.low_r);
  setTrackbarPos("Low H", "Object Detection", d.low_r);
}
void on_righ_r_thresh_trackbar(int, void *) {
  d.high_r = max(d.high_r, d.low_r + 1);
  setTrackbarPos("High H", "Object Detection", d.high_r);
}
void on_low_g_thresh_trackbar(int, void *) {
  d.low_g = min(d.high_g - 1, d.low_g);
  setTrackbarPos("Low S", "Object Detection", d.low_g);
}
void on_righ_g_thresh_trackbar(int, void *) {
  d.high_g = max(d.high_g, d.low_g + 1);
  setTrackbarPos("High S", "Object Detection", d.high_g);
}
void on_low_b_thresh_trackbar(int, void *) {
  d.low_b = min(d.high_b - 1, d.low_b);
  setTrackbarPos("Low B", "Object Detection", d.low_b);
}
void on_righ_b_thresh_trackbar(int, void *) {
  d.high_b = max(d.high_b, d.low_b + 1);
  setTrackbarPos("High B", "Object Detection", d.high_b);
}

int main() {
  angle_t a;
  struct timeval t1, t2;
  double dt;
  d.CreateGUI();

  while (char(waitKey(1)) != 'q') {
    gettimeofday(&t1, NULL);

    if (d.Detect(&a, 1) < 0)
      break;

    gettimeofday(&t2, NULL);

    dt = diff_timeval(t1, t2);
    printf("%8.4f, %8.4f in %8.4f s\n", a.psi, a.theta, dt);

    d.ShowFrames();
  }
  return 0;
}
