/*
 * Send the location of the ball over the network,
 * correlates it, and sends it to the controller.
 */

#include <cmath>
#include <cstdio>
#include <cstdlib>

#include "application.hpp"
#include "support.hpp"
#include "tcp.hpp"

#define DEBUG 0

#define GPMC_DEV "/dev/gpmcdev0"

void sink(int port) {
  angle_t a, b, c;
  measurement_t measure[2];
  setpoint_t set;

  // Open socket
  TCPServer socket(port);
  socket.Accept();

  // Initialise GPMC object
  GPMC gpmc(GPMC_DEV);

  // Run control loop
  while (char(waitKey(1)) != 'q') {
    // Read angle from socket
    if (socket.Read((char *)&a, sizeof(int)) == 0)
      break;

    // Read measurements
    gpmc.ReadMeasurements(measure, 2);

    // Convert angles
    b = MeasurementToAngle(measure[1], measure[0]);
    c = add_angle(a, b);
    set = AngleToSetpoint(c);

    // Set setpoint
    gpmc.SetSetpoints(&set, 1);

    if (DEBUG) {
      printf("Tilt measurement %i: encoder at: %i, error: %i, output: %i\n",
             measure[0].counter, measure[0].encoder, measure[0].error,
             measure[0].pwm);

      printf("Pan measurement %i: encoder at: %i, error: %i, output: %i\n",
             measure[1].counter, measure[1].encoder, measure[1].error,
             measure[1].pwm);

      printf("Ball angles: (%.2f, %.2f) = (%.2f, %.2f) + (%.2f, %.2f) \n",
             c.psi, c.theta, a.psi, a.theta, b.psi, b.theta);

      printf("Setpoints: pan %i, tilt %i\n", set.pan, set.tilt);
    }
  }

  socket.CloseAll();
}

void source(char *host, int port, int camN) {
  int e;
  angle_t a;
  struct timeval t1, t2;

  // Create and open socket
  TCPClient socket(host, port);

  // Initialise camera
  Detector cam(camN);

  // Run control loop
  while (char(waitKey(1)) != 'q') {
    // Detect frames
    e = cam.Detect(&a, 0);
    if (e < 0) {
      error("Error while detecting frame");
    }
    if (e == 0) {
      continue;
    }

    // Send angles over network
    socket.Write((char *)&a, sizeof(angle_t));

    if (DEBUG) {
      printf("Ball angles: (%.2f, %.2f) \n", a.psi, a.theta);
      printf("Time taken: %.4f\n", diff_timeval(t1, t2));
    }
  }

  socket.Close();
}

int main(int argc, char *argv[]) {
  int cam = 0;

  if (argc == 2) {
    sink(atoi(argv[1]));
  } else if (argc == 3 || argc == 4) {
    if (argc == 4) {
      cam = atoi(argv[3]);
    }
    source(argv[1], atoi(argv[2]), cam);
  } else {
    std::cerr << "Usage: " << argv[0] << " [address] port [cam]" << std::endl;
    exit(0);
  }

  return 0;
}
