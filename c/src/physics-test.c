#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "physics.h"

#define ANG_MAX 1
#define SAMPLES 50
#define CAM_DIST 5
#define FLOOR_Z -2

double rand_double(double max) {
  return ((double)rand() / (double)(RAND_MAX)) * max;
}

vector_t func_position(struct timeval t_init) {
  struct timeval t_curr;
  double t;
  vector_t p;

  gettimeofday(&t_curr, NULL);

  t = diff_timeval(t_init, t_curr);

  p.x = 5 * t;
  p.y = 2 * t - .1 * powf(t, 2);
  p.z = 10 - 0.5 * 9.81 * powf(t, 2);
  p.t = t_curr.tv_usec / 1000000.0;

  return p;
}

angle_t rand_angle() {
  angle_t a;

  a.psi = rand_double(ANG_MAX);
  a.theta = rand_double(ANG_MAX);

  return a;
}

angle_sample_t gen_angle_sample() {
  angle_sample_t sample;
  struct timeval tv;

  gettimeofday(&tv, NULL);

  sample.a[0] = rand_angle();
  sample.a[1] = rand_angle();
  sample.t = tv.tv_usec / 1000000.0;

  return sample;
}

void print_vector(vector_t v) {
  printf("     (%4.2f, %4.2f, %4.2f, %4.2f)", v.x, v.y, v.z, v.t);
}

int main() {
  vector_t v[SAMPLES], p[SAMPLES], a[SAMPLES], l[SAMPLES];
  struct timeval tv;

  /* Seed the RNG */
  srand(time(NULL));

  /* Set the initial time */
  gettimeofday(&tv, NULL);

  /* Generate angles, positions, velocities, accelerations */
  for (int i = 0; i < SAMPLES; i++) {
    p[i] = func_position(tv);

    if (i > 0) {
      v[i] = calc_velocity(p[i - 1], p[i]);
    }

    if (i > 1) {
      a[i] = calc_acceleration(v[i - 1], v[i]);
      l[i] = calc_landing(FLOOR_Z, p[i], v[i], a[i]);
    }

    if (p[i].z < FLOOR_Z) {
      break;
    }

    usleep(33333);
  }

  /* Show results */
  for (int i = 0; i < SAMPLES; i++) {
    print_vector(p[i]);

    if (i > 0) {
      print_vector(v[i]);
    }

    if (i > 1) {
      print_vector(a[i]);
      print_vector(l[i]);
    }

    printf("\n");

    if (p[i].z < FLOOR_Z) {
      break;
    }
  }

  return 0;
}
