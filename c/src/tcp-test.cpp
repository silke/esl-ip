#include <cmath>
#include <iostream>
#include <string>

#include "tcp.hpp"

void sink(int port) {
  int a;

  // Open socket
  TCPServer s(port);

  while (1) {
    s.Accept();
    std::cout << "Waiting for connection" << std::endl;

    // Run control loop
    while (1) {
      if (s.Read((char *)&a, sizeof(int)) == 0)
        break;

      std::cout << "a: " << a << std::endl;

      if (a == 0)
        break;
    }

    // Write reply
    std::cout << "write: " << a << std::endl;
    s.Write((char *)&a, sizeof(int));

    // Close sockets
    s.Close();
  }

  s.CloseAll();
}

void source(char *host, int port) {
  int a;
  srand(time(NULL));

  // Create and open socket
  std::cout << "Opening connection" << std::endl;
  TCPClient c(host, port);

  // Run control loop
  for (int i = 0; i < 10; i++) {
    a = rand();
    std::cout << "a: " << a << std::endl;

    // Send angle over network
    c.Write((char *)&a, sizeof(int));
  }

  // Write a 0 to stop!
  a = 0;
  c.Write((char *)&a, sizeof(int));

  // Read the reply (should be zero again)
  c.Read((char *)&a, sizeof(int));

  // Show reply
  std::cout << "a: " << a << std::endl;

  // Close socket
  c.Close();
}

int main(int argc, char *argv[]) {
  if (argc == 2) {
    sink(atoi(argv[1]));
  } else if (argc == 3) {
    source(argv[1], atoi(argv[2]));
  } else {
    std::cerr << "Usage: " << argv[0] << " [address] port" << std::endl;
    exit(0);
  }

  return 0;
}
