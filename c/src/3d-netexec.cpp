#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "application.hpp"
#include "support.hpp"
#include "tcp.hpp"

#define GPMC_DEV "/dev/gpmcdev0"
#define GPMC_ENABLE
#define DEBUG 0

#define SW 12
#define SD 4

#define CAM_DIST 1.0
#define FLOOR_Z -1.0

typedef struct {
  measurement_t tilt;
  measurement_t pan;
} cam_pos_t;

typedef struct { setpoint_t a, b; } setpoints_t;

typedef struct {
  double t;
  angle_t a[2], b[2], c[2];
  cam_pos_t m[2];
  setpoint_t s[2];
  vector_t p, v, ac, l;
} transfer_t;

void setup(GPMC *gpmc) {
  cam_controller_settings_t c[2];
  setpoint_t s[2];

  // PWM settings
  c[0].pan.pwm.limit = 2500;
  c[0].pan.pwm.top = 2500;
  c[0].pan.pwm.lead = 10;
  c[0].tilt.pwm.limit = 2500;
  c[0].tilt.pwm.top = 2500;
  c[0].tilt.pwm.lead = 10;

  // PID settings
  c[0].pan.pid.kp = tofixed(13.0);
  c[0].pan.pid.ki = tofixed(0.0);
  c[0].pan.pid.kd = tofixed(3000.0);
  c[0].tilt.pid.kp = tofixed(3.2);
  c[0].tilt.pid.ki = tofixed(0.0);
  c[0].tilt.pid.kd = tofixed(3000.0);

  // Use same settings for both cams
  c[1] = c[0];

  // Write settings
  gpmc->Configure(c, 2);

  // Write setpoints to zero
  s[0].pan = 0;
  s[0].tilt = 0;
  s[1].pan = 0;
  s[1].tilt = 0;
  gpmc->SetSetpoints(s, 2);
}

void source(char *host, int port) {
  int e[2];
  transfer_t t;
  vector_t p, v;
  Detector cam[2];
  struct timeval t1, t2;

  // Create and open socket
  TCPClient socket(host, port);

#ifdef GPMC_ENABLE
  // Initialise object
  GPMC gpmc(GPMC_DEV);
  setup(&gpmc);
#endif

  // Initialise camera
  cam[0] = Detector(0);
  cam[1] = Detector(1);

  // Set the initial time
  gettimeofday(&t1, NULL);

  // Run control loop
  while (char(waitKey(1)) != 'q') {
    for (int i = 0; i < 2; i++) {
      // Detect ball from camera 1
      e[i] = cam[i].Detect(&t.a[i], 0);
      if (e[i] < 0) {
        error("Error while detecting frame");
      }
    }

    if (DEBUG) {
      printf("Sizes: %i, %i\n", e[0], e[1]);
    }

    // Skip when no ball
    if (e[0] == 0 && e[1] == 0) {
      continue;
    }

    // Get the current time
    gettimeofday(&t2, NULL);
    t.t = diff_timeval(t1, t2);

#ifdef GPMC_ENABLE
    // Read measurements
    gpmc.ReadMeasurements((measurement_t *)t.m, 4);
#endif

    // Convert angles
    for (int i = 0; i < 2; i++) {
      t.b[i] = MeasurementToAngle(t.m[i].pan, t.m[i].tilt);
      t.c[i] = add_angle(t.a[i], t.b[i]);

      // Change setpoint when the ball was in frame
      if (e[0] != 0) {
        t.s[i] = AngleToSetpoint(t.c[i]);
      }
    }

#ifdef GPMC_ENABLE
    // Set setpoint
    gpmc.SetSetpoints(t.s, 2);
#endif

    // Skip unless both cameras see the ball
    if (e[0] == 0 || e[1] == 0) {
      continue;
    }

    // Calculate position
    t.p = calc_position_with_time(CAM_DIST, t.a, t.t);
    t.v = calc_velocity(p, t.p);
    t.ac = calc_acceleration(v, t.v);
    t.l = calc_landing(FLOOR_Z, t.p, t.v, t.ac);

    // Save last values
    p = t.p;
    v = t.v;

    // Write to network
    socket.Write((char *)&t, sizeof(transfer_t));
  }

  socket.Close();
#ifdef GPMC_ENABLE
  gpmc.Close();
#endif
}

void print_vector(vector_t v) {
  printf("(%7.2f,%7.2f,%7.2f,%7.2f) ", v.x, v.y, v.z, v.t);
}

void print_angles(angle_t a[2]) {
  printf("(%5.2f,%5.2f/%5.2f,%5.2f) ", a[0].psi, a[0].theta, a[1].psi,
         a[1].theta);
}

void sink(int port) {
  transfer_t t;
  unsigned int i = 0;

  // Open socket
  TCPServer socket(port);
  socket.Accept();

  while (true) {
    // Read angle from socket
    if (socket.Read((char *)&t, sizeof(transfer_t)) == 0)
      break;

    print_angles(t.a);
    print_angles(t.b);
    print_angles(t.c);
    print_vector(t.p);
    print_vector(t.v);
    print_vector(t.ac);
    print_vector(t.l);
    printf("\n");

    i++;
  }

  socket.CloseAll();
}

int main(int argc, char *argv[]) {
  if (argc == 2) {
    sink(atoi(argv[1]));
  } else if (argc == 3) {
    source(argv[1], atoi(argv[2]));
  } else {
    std::cerr << "Usage: " << argv[0] << " [address] port" << std::endl;
    exit(0);
  }

  return 0;
}
